# Actuator

```md
- add below dependency to activate actautor
        <dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
		</dependency>
```

## Notes

```
1. By adding above dependency, spring boot auto enable heath check endpoints .
   (on checking logs look for **Exposing 1 endpoint(s) beneath base path '/actuator'**).
  -- to find out the exposed endpoint(s) at any points use **{baseEndpoint}/actuator**
     eg. GET http://127.0.0.1:8080/actuator

2. There are some other endpoints that we need to enable by making some configuration,
   these are not enabled by spring boot as it contains sensitive information.

3. We can enable sensitive endpoint on config it in **application.properties** file
  - 3.1
  - to config include/exclude endpoints
    -- management.endpoints.<id>.exposure.<include/exclude>
    -- refer **application.properties** file

  - 3.2
  - to config health endpoints(GET http://127.0.0.1:8080/actuator/health)
    -- by default basic health endpoint response in returned by spring boot.
    -- to get addition info use below property in application properties file
    -- **management.endpoint.health.show-details = always**
    -- refer **application.properties** file

  - 3.3
  - to config info endpoint(GET http://127.0.0.1:8080/actuator/info)
    -- it will return app info, by default it returns empty json
    -- to do that add properties in **application.properties** with prefix as info.
    -- e.g. info.app.name = Basic Actuator Application
    -- refer **application.properties** file.

```

## References

```
1. https://docs.spring.io/spring-boot/docs/2.0.x/reference/html/production-ready-enabling.html
2. https://chrome.google.com/webstore/detail/json-viewer -  to download json viewer plugin for chrome
```