package com.poc.actuator.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "/test")
public class TestController {

    @GetMapping
    public ResponseEntity<?> test(@RequestParam(required = false) Integer status){
        if(ObjectUtils.isEmpty(status)){
            status = Integer.valueOf(200);
        }
        Map<String, Object> res = new HashMap<>();
        if(status == HttpStatus.OK.value()){
            res.put("status", HttpStatus.OK);
            res.put("statusCode", status);
            return ResponseEntity.ok(res);
        }else if(status == HttpStatus.CREATED.value()){
            HttpStatus resStatus = HttpStatus.CREATED;
            res.put("status", resStatus);
            res.put("statusCode", status);
            return ResponseEntity
                    .status(resStatus)
                    .body(res);
        }else if(status == HttpStatus.BAD_REQUEST.value()){
            HttpStatus resStatus = HttpStatus.BAD_REQUEST;
            res.put("status", HttpStatus.OK);
            res.put("statusCode", status);
            return ResponseEntity
                    .status(resStatus)
                    .body(res);
        }else{
            HttpStatus resStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            res.put("status", HttpStatus.OK);
            res.put("statusCode", status);
            return ResponseEntity
                    .status(resStatus)
                    .body(res);
        }
    }
}
